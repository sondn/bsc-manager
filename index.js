var Web3 = require('web3');


class BscManager {

    constructor(rpcUrl) {
        this.web3 = new Web3(new Web3.providers.HttpProvider(rpcUrl));
    }

    importWalletByPrivateKey(privateKey) {
        const account = this.web3.eth.accounts.privateKeyToAccount(privateKey);
        let wallet = this.web3.eth.accounts.wallet.add(account);
        let keystore = wallet.encrypt(this.web3.utils.randomHex(32));
        const responsse = {
            account: account,
            wallet: wallet,
            keystore: keystore,
        };

        return responsse;
    }

    async getBnbBalance(address) {
        // Get Balance
        let balance = await this.web3.eth.getBalance(address);
        return balance / Math.pow(10,18);
    }
    
    async getBEPTokenBalance(tokenAddress , address) {
        // ABI to transfer ERC20 Token
        let abi = bep20ABI;
        // Get ERC20 Token contract instance
        let contract = new this.web3.eth.Contract(abi, tokenAddress);
        // Get decimal
        let decimal = await contract.methods.decimals().call();
        // Get Balance
        let balance = await contract.methods.balanceOf(address).call();
        // Get Name
        // let name = await contract.methods.name().call();
        // Get Symbol
        // let symbol = await contract.methods.symbol().call();
       
        return balance / Math.pow(10,decimal);
    }


    async getBEPTokenInfo(tokenAddress) {
        // ABI to transfer ERC20 Token
        let abi = bep20ABI;
        // Get ERC20 Token contract instance
        let contract = new this.web3.eth.Contract(abi, tokenAddress);
        // Get decimal
        let decimal = await contract.methods.decimals().call();
        // Get Name
        let name = await contract.methods.name().call();
        // Get Symbol
        let symbol = await contract.methods.symbol().call();
       
        return {
            contract_address: tokenAddress,
            name: name,
            symbol: symbol,
            decimal: decimal
        }
    }

    async getBEPCurrentBlock(){
        return this.web3.eth.getBlockNumber();
    }
    
    async getBEPTransactions(address, startBlockNumber, endBlockNumber) {
        address = address.toLowerCase();
        var res = [];
        if (endBlockNumber == null) {
            endBlockNumber = await this.web3.eth.getBlockNumber();
        }
        if (startBlockNumber == null) {
            startBlockNumber = endBlockNumber - 5;
        }
        console.log("Searching for transactions to/from account \"" + address + "\" within blocks "  + startBlockNumber + " and " + endBlockNumber);

        var arrPromises = [];
        for (var i = startBlockNumber; i <= endBlockNumber; i++) {
            arrPromises.push(this.getBEPTransactionsFromBlock(address, i));
        }

        return Promise.all(arrPromises).then((data) => {
            var res =  [].concat.apply([], data);
            return res;
        });
    }


    async getBEPTransactionsFromBlock(address, block){
        try{
            var result = [];
            var res = await this.web3.eth.getBlock(block, true);
            if (res != null && res.transactions != null) {    
                res.transactions.forEach( function(e) {
                    if(e.from != null && e.to != null){
                        if (address == e.from.toLowerCase() || address == e.to.toLowerCase()) {
                            result.push(e);
                        }
                    }
                });
            }
            return result;
        } catch (e) {
            console.log(e);
        }
              
        
    }


    async sendBEPToken(privateKey, tokenContractAddress , toAddress , amount , chainId) {
        let account = this.web3.eth.accounts.privateKeyToAccount(privateKey);
        let wallet = this.web3.eth.accounts.wallet.add(account);
        // ABI to transfer ERC20 Token
        let abi = bep20ABI;
        // calculate ERC20 token amount
        let tokenAmount = this.web3.utils.toWei(amount.toString(), 'ether')
        // Get ERC20 Token contract instance
        let contract = new this.web3.eth.Contract(abi, tokenContractAddress, {from: wallet.address});
        const data = await contract.methods.transfer(toAddress, tokenAmount).encodeABI();
        // The gas price is determined by the last few blocks median gas price.
        const gasPrice = await this.web3.eth.getGasPrice();
        const gasLimit = 90000;
        // Build a new transaction object.
        const rawTransaction = {
            'from': wallet.address,
            'nonce': this.web3.utils.toHex(this.web3.eth.getTransactionCount(wallet.address)),
            'gasPrice': this.web3.utils.toHex(gasPrice),
            'gasLimit': this.web3.utils.toHex(gasLimit),
            'to': tokenContractAddress,
            'value': 0,
            'data': data,
            'chainId': chainId
        };
        const res = await contract.methods.transfer(toAddress, tokenAmount).send({
            from: wallet.address,
            gas: 150000
        });

        // // Get Name
        // let name = await contract.methods.name().call();
        // // Get Symbol
        // let symbol = await contract.methods.symbol().call();

        return res;
    }

}

exports.getBscManager = (rpcUrl) => {
    return new BscManager(rpcUrl);
}

let bep20ABI = [
    {
        "constant": true,
        "inputs": [],
        "name": "name",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_spender",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "approve",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "totalSupply",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_from",
                "type": "address"
            },
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transferFrom",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "decimals",
        "outputs": [
            {
                "name": "",
                "type": "uint8"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            }
        ],
        "name": "balanceOf",
        "outputs": [
            {
                "name": "balance",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "symbol",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_spender",
                "type": "address"
            }
        ],
        "name": "allowance",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "owner",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "spender",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Approval",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "from",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Transfer",
        "type": "event"
    }
];