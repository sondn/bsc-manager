### Install

npm install @sondn.contact/bsc-manager

### Useage

```
	const manager = require('@sondn.contact/bsc-manager');

	// rpcUrl BSC Testnet: https://data-seed-prebsc-1-s1.binance.org:8545

	var bscManager = manager.getBscManager(rpcUrl);

	bscManager.getBnbBalance("your address").then(res => console.log);

```